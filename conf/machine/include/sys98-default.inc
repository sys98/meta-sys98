#
# Ethernet, Wi-Fi, and Bluetooth configuration used in recipes
#
# ETHx_MODE, WLANx_MODE accepted values: 'dhcp' or 'static'
#
ETH0_STATIC_IP       ?= "192.168.42.30"
ETH0_STATIC_NETMASK  ?= "255.255.255.0"
ETH0_STATIC_GATEWAY  ?= "192.168.42.1"

WLAN0_STATIC_IP       ?= "192.168.1.24"
WLAN0_STATIC_NETMASK  ?= "255.255.255.0"
WLAN0_STATIC_GATEWAY  ?= "192.168.1.1"
WLAN0_SSID            ?= "Vinaphone"
WLAN0_PSK             ?= "1234512345"

#
# Enable debugging using ADB
#
DEBUGGING_ENABLED = "0"

QT_ENABLED = "0"

LXC_ENABLED = "0"
