#
# Copyright (C) 2022 Sys98.
#
SUMMARY = "Additional QT package for host machine"

inherit packagegroup

RDEPENDS_${PN} += "\
    qtvirtualkeyboard \
    qtvirtualkeyboard-dev \
"
