SUMMARY = "A console-only image that fully supports the target device \
hardware."

IMAGE_FEATURES += "ssh-server-dropbear splash hwcodecs"

#CORE_IMAGE_EXTRA_INSTALL += " \
#    wayland \
#    weston \
#"

IMAGE_INSTALL_append = " \
    packagegroup-sys98-core \
"

IMAGE_INSTALL_append = "${@oe.utils.conditional('DEBUGGING_ENABLED', '1', ' packagegroup-sys98-debug ', '', d)}"
IMAGE_INSTALL_append = "${@oe.utils.conditional('QT_ENABLED', '1', ' psplash packagegroup-sys98-qt5 ', '', d)}"
IMAGE_INSTALL_append = "${@oe.utils.conditional('LXC_ENABLED', '1', ' packagegroup-sys98-lxc ', '', d)}"

SYSTEMD_AUTO_ENABLE_DHCP = "no"

inherit core-image

