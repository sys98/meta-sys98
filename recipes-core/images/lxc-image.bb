SUMMARY = "A container console-only image that can be loaded inside host image using LXC"

IMAGE_FSTYPES = "ext4"

IMAGE_INSTALL_append = " \
    packagegroup-sys98-core \
"

inherit core-image
