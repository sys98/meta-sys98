#
# Copyright (C) 2022 Sys98.
#
SUMMARY = "QT5 applications packagegroup for Sys98 image"

inherit packagegroup

RDEPENDS_${PN} = "\
    rsync \
    weston \
    weston-init \
    liberation-fonts \
    qtbase \
    qtbase-plugins \
    qtdeclarative \
    qtquickcontrols \
    qtquickcontrols2 \
    qtlocation \
    qtvirtualkeyboard \
    qtvirtualkeyboard-plugins \
    qtgraphicaleffects \
    qtwayland-plugins \
"

PACKAGECONFIG_append_pn-qtbase = " wayland virtualkeyboard"

PACKAGECONFIG_remove_pn-qtbase = "ptest"

