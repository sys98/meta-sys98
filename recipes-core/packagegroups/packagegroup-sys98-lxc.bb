#
# Copyright (C) 2022 Sys98.
#
SUMMARY = "LXC applications packagegroup for Sys98 image"

inherit packagegroup

RDEPENDS_${PN} = " \
    lxc \
    lxc-config \
"
