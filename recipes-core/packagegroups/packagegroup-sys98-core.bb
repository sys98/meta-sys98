#
# Copyright (C) 2022 Sys98.
#
SUMMARY = "Core applications packagegroup for Sys98 image"

inherit packagegroup

RDEPENDS_${PN} = "\
    ${@bb.utils.contains('MACHINE_FEATURES', 'wifi', 'wpa-supplicant', '', d)} \
    ${@bb.utils.contains('MACHINE_FEATURES', 'bluetooth', 'bluez5', '', d)} \
"
