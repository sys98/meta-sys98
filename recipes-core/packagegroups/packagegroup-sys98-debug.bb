#
# Copyright (C) 2022 Sys98.
#
SUMMARY = "Debug applications packagegroup for Sys98 image"

inherit packagegroup

RDEPENDS_${PN} = "\
    dlt-daemon \
    wpa-supplicant \
    android-tools \
"
