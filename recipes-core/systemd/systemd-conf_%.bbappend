FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += " \
    file://20-wired.network \
    file://10-wireless.network \
"

FILES_${PN} += " \
    ${sysconfdir}/systemd/network/20-wired.network \
    ${sysconfdir}/systemd/network/10-wireless.network \
"

#
# Convert IP addresses to CIDR format
#
def ipaddr_to_cidr(iface, d):
    ipaddr = d.getVar('%s_STATIC_IP' % iface.upper(), True)
    netmask = d.getVar('%s_STATIC_NETMASK' % iface.upper(), True)
    binary_str = ''
    for byte in netmask.split('.'):
        binary_str += bin(int(byte))[2:].zfill(8)
    return ipaddr + '/' + str(len(binary_str.rstrip('0')))

def get_default_gateway(iface, d):
    gateway = d.getVar('%s_STATIC_GATEWAY' % iface.upper(), True)
    return gateway


ETH0_STATIC_CIDR = "${@ipaddr_to_cidr('eth0', d)}"
ETH0_STATIC_GW   = "${@get_default_gateway('eth0', d)}"

WLAN0_STATIC_CIDR = "${@ipaddr_to_cidr('wlan0', d)}"
WLAN0_STATIC_GW   = "${@get_default_gateway('wlan0', d)}"

do_install_append() {
    install -d ${D}${sysconfdir}/systemd/network
    install -m 0644 ${WORKDIR}/20-wired.network ${D}${sysconfdir}/systemd/network/20-wired.network
    install -m 0644 ${WORKDIR}/10-wireless.network ${D}${sysconfdir}/systemd/network/10-wireless.network

    sed -i -e "s,##ETH0_STATIC_CIDR##,${ETH0_STATIC_CIDR},g" \
        ${D}${sysconfdir}/systemd/network/20-wired.network
    sed -i -e "s,##ETH0_STATIC_GW##,${ETH0_STATIC_GW},g" \
        ${D}${sysconfdir}/systemd/network/20-wired.network

    sed -i -e "s,##WLAN0_STATIC_CIDR##,${WLAN0_STATIC_CIDR},g" \
        ${D}${sysconfdir}/systemd/network/10-wireless.network
    sed -i -e "s,##WLAN0_STATIC_GW##,${WLAN0_STATIC_GW},g" \
        ${D}${sysconfdir}/systemd/network/10-wireless.network
}

