SUMMARY = "A console-only image that fully supports the target device hardware."
LICENSE = "CLOSED"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

inherit systemd

SYSTEMD_AUTO_ENABLE = "enable"
SYSTEMD_SERVICE_${PN} = "lxc-config.service"

SRC_URI += " \
    file://lxc-config.service \
    file://init-lxc.sh \
    file://lxc.linux1.conf \
"

FILES_${PN} += "${systemd_unitdir}/system/lxc-config.service"

do_install() {
    install -d ${D}/var/lib/lxc
    install -m 0755 ${WORKDIR}/init-lxc.sh ${D}/var/lib/lxc
    install -m 0644 ${WORKDIR}/lxc.linux1.conf ${D}/var/lib/lxc

    install -d ${D}/${systemd_unitdir}/system
    install -m 0644 ${WORKDIR}/lxc-config.service ${D}/${systemd_unitdir}/system
}

RDEPENDS_${PN} += "bash"
