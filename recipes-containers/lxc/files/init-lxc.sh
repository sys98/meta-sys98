#!/bin/bash

LOG_TAG="[$(basename $0)]"
LXC_PATH="/var/lib/lxc"

# Define the container name
CONTAINER_NAME="linux1"

# Check if the container exists
if ! lxc-info -n ${CONTAINER_NAME} >/dev/null 2>&1; then
    # Create container
    echo "${LOG_TAG} the container ${CONTAINER_NAME} doesn't exists, creating it..." >> /dev/kmsg
    lxc-create -n ${CONTAINER_NAME} -f ${LXC_PATH}/lxc.linux1.conf -t none

    echo "${LOG_TAG} running ${CONTAINER_NAME} container..." >> /dev/kmsg
    lxc-start -n ${CONTAINER_NAME}
fi
