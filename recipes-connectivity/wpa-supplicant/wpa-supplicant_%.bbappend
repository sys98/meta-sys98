FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += "file://wpa_supplicant-nl80211-wlan0.conf"

SYSTEMD_AUTO_ENABLE = "enable"
SYSTEMD_SERVICE_${PN}_append = " wpa_supplicant-nl80211@wlan0.service  "

def get_ssid(iface, d):
    ssid = d.getVar('%s_SSID' % iface.upper(), True)
    return ssid

def get_psk(iface, d):
    psk = d.getVar('%s_PSK' % iface.upper(), True)
    return psk

SSID = "${@get_ssid('wlan0', d)}"
PSK  = "${@get_psk('wlan0', d)}"

do_install_append () {
   install -d ${D}${sysconfdir}/wpa_supplicant/
   install -D -m 600 ${WORKDIR}/wpa_supplicant-nl80211-wlan0.conf ${D}${sysconfdir}/wpa_supplicant/wpa_supplicant-nl80211-wlan0.conf

   install -d ${D}${sysconfdir}/systemd/system/multi-user.target.wants/
   ln -s ${systemd_unitdir}/system/wpa_supplicant@.service ${D}${sysconfdir}/systemd/system/multi-user.target.wants/wpa_supplicant-nl80211@wlan0.service

   sed -i -e "s,##SSID##,${SSID},g" \
        ${D}${sysconfdir}/wpa_supplicant/wpa_supplicant-nl80211-wlan0.conf
   sed -i -e "s,##PSK##,${PSK},g" \
        ${D}${sysconfdir}/wpa_supplicant/wpa_supplicant-nl80211-wlan0.conf 
}