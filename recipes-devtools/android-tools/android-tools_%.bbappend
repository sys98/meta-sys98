FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += "file://usb-debugging-enabled"

FILES_${PN} += "${localstatedir}/usb-debugging-enabled"

do_install_append() {
   install -d ${D}${localstatedir}
   install -D -m 0600 ${WORKDIR}/usb-debugging-enabled ${D}${localstatedir}/usb-debugging-enabled
}
